// ==============================================================
//                 ORBITER MODULE: MOL1
//                  Part of the ORBITER SDK
//          Copyright (C) 2002-2004 Martin Schweiger
//                   All rights reserved
//
// MOL1.cpp
// Control module for MOL1 vessel class
//

// ==============================================================
#define ORBITER_MODULE
#include "orbitersdk.h"
#include "ORIONSERVICEA.h"









VISHANDLE MainExternalMeshVisual = 0;






// Constructor
ORIONSERVICEA::ORIONSERVICEA(OBJHANDLE hObj, int fmodel)
: VESSEL3(hObj, fmodel)
{




}
void ORIONSERVICEA::initGlobalVars(){
	
	GetCurrentDirectory(MAXLEN, OrbiterRoot); //initialize orbiter root with the current directory
	


	

	
	

	
	return;
}
void ORIONSERVICEA::ResetVehicle()
{
	ClearMeshes();
	ClearAttachments();
	initGlobalVars();

	char tempFile[MAXLEN];
	strcpy(tempFile, OrbiterRoot);
	strcat(tempFile, "\\");
	strcat(tempFile, fileini);
	sprintf(logbuff, "%s: Config File: %s", GetName(), tempFile);
	oapiWriteLog(logbuff);
	parseinifile(tempFile);

	
	currentPayload = loadedCurrentPayload;
	
	Configuration = loadedConfiguration;
	


	if (Configuration == 0){			//if only configuration is defined, reset everything
		
		currentPayload = 0;
		
	}

	
	//VehicleSetup();
	LoadMeshes();

	//vs2.arot=_V(0,0,0);
	vs2.rvel = _V(0, 0, 0);

	clbkSetStateEx(&vs2);

	

	clbkPostCreation();


	return;
}


//
void ORIONSERVICEA::clbkSetClassCaps(FILEHANDLE cfg)

{
	// physical specs
	SetSize(5);
	
	SetEmptyMass(11476);
	SetCW(0.3, 0.3, 0.6, 0.9);
	SetWingAspect(0.1);
	SetWingEffectiveness(0.1);
	SetCrossSections(_V(950, 838.24, 1080));
	SetRotDrag(_V(0.1, 0.1, 0.1));
	if (GetFlightModel() >= 1) {
		SetPitchMomentScale(1e-4);
		SetBankMomentScale(1e-4);
	}
	SetPMI(_V(.88, .99, .84));
	SetTrimScale(0.05);
	SetCameraOffset(_V(0, .2, 3.79));

	
	EnableTransponder(true);

	SetTouchdownPoints(_V(0, 0, 22), _V(-20, 0, -22), _V(20, 0, -22));;



	SetMeshVisibilityMode(AddMesh(oapiLoadMeshGlobal("SLS_2016/ORIONSERVICEADAPTER1")), MESHVIS_ALWAYS); //Main ship mesh



}

void ORIONSERVICEA::clbkPreStep(double SimT, double SimDT, double MJD) {





}


char* ORIONSERVICEA::GetProperPayloadMeshName(int pnl, int n){
	if (n == 0){
		return payload[pnl].meshname0;
	}
	else if (n == 1){
		return payload[pnl].meshname1;
	}
	else if (n == 2){
		return payload[pnl].meshname2;
	}
	else if (n == 3){
		return payload[pnl].meshname3;
	}
	else if (n == 4){
		return payload[pnl].meshname4;
	}
	else{
		return payload[pnl].meshname0;
	}
}
void ORIONSERVICEA::clbkPostStep(double simt, double simdt, double mjd)
{




}






DLLCLBK VESSEL *ovcInit(OBJHANDLE hvessel, int flightmodel)
{
	return new ORIONSERVICEA(hvessel, flightmodel);
}

DLLCLBK void ovcExit(VESSEL *vessel)
{
	if (vessel) delete (ORIONSERVICEA*)vessel;
}
MATRIX3 ORIONSERVICEA::RotationMatrix(VECTOR3 angles)
{
	MATRIX3 m;
	MATRIX3 RM_X, RM_Y, RM_Z;
	RM_X = _M(1, 0, 0, 0, cos(angles.x), -sin(angles.x), 0, sin(angles.x), cos(angles.x));
	RM_Y = _M(cos(angles.y), 0, sin(angles.y), 0, 1, 0, -sin(angles.y), 0, cos(angles.y));
	RM_Z = _M(cos(angles.z), -sin(angles.z), 0, sin(angles.z), cos(angles.z), 0, 0, 0, 1);
	m = mul(RM_Z, mul(RM_Y, RM_X));
	return m;
}
VECTOR3 ORIONSERVICEA::RotateVector(const VECTOR3& input, double angle, const VECTOR3& rotationaxis)
{
	// To rotate a vector in 3D space we'll need to build a matrix, these are the variables treqired to do so.
	MATRIX3 rMatrix;
	double c = cos(angle);
	double s = sin(angle);
	double t = 1.0 - c;
	double x = rotationaxis.x;
	double y = rotationaxis.y;
	double z = rotationaxis.z;

	// Build rotation matrix
	rMatrix.m11 = (t * x * x + c);
	rMatrix.m12 = (t * x * y - s * z);
	rMatrix.m13 = (t * x * z + s * y);
	rMatrix.m21 = (t * x * y + s * z);
	rMatrix.m22 = (t * y * y + c);
	rMatrix.m23 = (t * y * z - s * x);
	rMatrix.m31 = (t * x * z - s * y);
	rMatrix.m32 = (t * y * z + s * x);
	rMatrix.m33 = (t * z * z + c);

	// Perform Rotation
	VECTOR3 output = mul(rMatrix, input); // multiply the input vector by our rotation matrix to get our output vector
	return output; // Return rotated vector
}
void ORIONSERVICEA::RotatePayload(int pns, int nm, VECTOR3 anglesrad){

	VECTOR3 state = _V(anglesrad.x / (2 * PI), anglesrad.y / (2 * PI), anglesrad.z / (2 * PI));//portion of 2*PI to rotate
	VECTOR3 reference;

	if (nm == 0){ reference = _V(0, 0, 0); }
	else{ reference = operator-(payload[pns].off[nm], payload[pns].off[0]); }

	{
		SetAnimation(RotatePayloadAnim_z[pns][nm], 0);
		SetAnimation(RotatePayloadAnim_y[pns][nm], 0);
		SetAnimation(RotatePayloadAnim_x[pns][nm], 0);

		/*DelAnimationComponent(RotatePayloadAnim_z[pns][nm],anim_z[pns][nm]);
		DelAnimationComponent(RotatePayloadAnim_y[pns][nm],anim_y[pns][nm]);
		DelAnimationComponent(RotatePayloadAnim_x[pns][nm],anim_x[pns][nm]);*/

	}


	SetAnimation(RotatePayloadAnim_x[pns][nm], state.x);
	SetAnimation(RotatePayloadAnim_y[pns][nm], state.y);
	SetAnimation(RotatePayloadAnim_z[pns][nm], state.z);


	return;
}

//Load Meshes
void ORIONSERVICEA::LoadMeshes(){

	



	for (int pns = currentPayload; pns<nPayloads; pns++){

		if (!payload[pns].live){
			for (int nm = 0; nm<payload[pns].nMeshes; nm++){

				VECTOR3 pos = payload[pns].off[nm];
				payload[pns].msh_h[nm] = oapiLoadMeshGlobal(GetProperPayloadMeshName(pns, nm));
				sprintf(logbuff, "%s Payload Mesh Preloaded %i", GetName(), pns + 1);
				oapiWriteLog(logbuff);
				payload[pns].msh_idh[nm] = AddMesh(payload[pns].msh_h[nm], &pos);
				sprintf(logbuff, "%s: Payload n.%i Mesh Added: %s @ x:%.3f y:%.3f z:%.3f", GetName(), pns + 1, GetProperPayloadMeshName(pns, nm), pos.x, pos.y, pos.z);
				oapiWriteLog(logbuff);
				if (payload[pns].render == 0) {
					SetMeshVisibilityMode(payload[pns].msh_idh[nm], MESHVIS_NEVER);
				}
				//if(payload[pns].rotated){
				RotatePayload(pns, nm, payload[pns].Rotation);
				//	}


				/// ATTACHMENT POINTS MUST BE CREATED ANYWAY TO GET THE PAYLOAD BACK ONCE RELOADING THE SCENARIO
			}
		}//else{
		VECTOR3 direction, normal;
		if (!payload[pns].rotated){ direction = _V(0, 0, 1); normal = _V(0, 1, 0); }
		else{
			direction = payload[pns].Rotation;
			VECTOR3 rotation;
			rotation = payload[pns].Rotation;
			direction = mul(RotationMatrix(rotation), _V(0, 0, 1));
			normal = mul(RotationMatrix(rotation), _V(0, 1, 0));
			normalise(normal);
			normalise(direction);
		}

		live_a[pns] = CreateAttachment(false, payload[pns].off[0], direction, normal, "MS2015", false);

		//}
	}
	

	return;
}

void ORIONSERVICEA::UpdateLivePayloads(){
	/*
	for (int pns = currentPayload; pns<nPayloads; pns++){
		if (payload[pns].live){
			VESSELSTATUS2 vslive;
			memset(&vslive, 0, sizeof(vslive));
			vslive.version = 2;
			OBJHANDLE checklive = oapiGetVesselByName(payload[pns].name);
			if (oapiIsVessel(checklive)){
				ATTACHMENTHANDLE liveatt;
				VESSEL3 *livepl;
				livepl = (VESSEL3*)oapiGetVesselInterface(checklive);
				liveatt = livepl->CreateAttachment(TRUE, _V(0, 0, 0), _V(0, 0, -1), _V(0, 1, 0), "MS2015", FALSE);
				AttachChild(checklive, live_a[pns], liveatt);
				if (payload[pns].mass <= 0){
					payload[pns].mass = livepl->GetMass();
				}
				if (payload[pns].height <= 0){
					payload[pns].height = livepl->GetSize();
					payload[pns].diameter = payload[pns].height*0.1;
				}
			}
			else{

				VESSEL3 *v;
				OBJHANDLE hObj;
				ATTACHMENTHANDLE liveatt;
				GetStatusEx(&vslive);
				hObj = oapiCreateVesselEx(payload[pns].name, payload[pns].module, &vslive);


				if (oapiIsVessel(hObj)){
					v = (VESSEL3*)oapiGetVesselInterface(hObj);

					liveatt = v->CreateAttachment(TRUE, _V(0, 0, 0), _V(0, 0, -1), _V(0, 1, 0), "MS2015", FALSE);

					AttachChild(hObj, live_a[pns], liveatt);
					if (payload[pns].mass <= 0){
						payload[pns].mass = v->GetMass();
					}
					if (payload[pns].height <= 0){
						payload[pns].height = v->GetSize();
						payload[pns].diameter = payload[pns].height*0.1;
					}
				}
			}
		}
	}
	*/
	return;
}

// --------------------------------------------------------------
// Keyboard interface handler (buffered key events)
// --------------------------------------------------------------
int ORIONSERVICEA::clbkConsumeBufferedKey(DWORD key, bool down, char *kstate)
{
	// only process keydown events
	if (!down)
		return 0;



	if (key == OAPI_KEY_J)
	{

		
		return 1;
	}
	

	return 0;

}




void ORIONSERVICEA::clbkSaveState(FILEHANDLE scn)
{

	char savebuff[256], savevalbuff[256];
	
	sprintf(savebuff, "CURRENT_PAYLOAD");
	sprintf(savevalbuff, "%i", currentPayload + 1);
	oapiWriteScenario_string(scn, savebuff, savevalbuff);



	SaveDefaultState(scn);

}
void ORIONSERVICEA::clbkLoadStateEx(FILEHANDLE scn, void *status)
{
	char *line;
	while (oapiReadScenario_nextline(scn, line))
	{

	

	}

	char tempFile[MAXLEN];
	strcpy(tempFile, OrbiterRoot);
	strcat(tempFile, "\\");
	strcat(tempFile, fileini);
	sprintf(logbuff, "%s: Config File: %s", GetName(), tempFile);
	oapiWriteLog(logbuff);
	parseinifile(tempFile);
}

VECTOR3 ORIONSERVICEA::CharToVec(char charvar[MAXLEN], VECTOR3* outvec){

	double dbuff[3];
	char *cbuff;
	cbuff = NULL;
	cbuff = strtok(charvar, ",");
	int k = 0;
	while (cbuff != NULL)
	{
		int cbs;
		for (cbs = 0; cbs<sizeof(cbuff); cbs++){
			if (cbuff[cbs] == '(') cbuff[cbs] = ' ';
			else if (cbuff[cbs] == ')') cbuff[cbs] = ' ';
		}


		dbuff[k] = atof(cbuff);

		switch (k){
		case 0:
			outvec->x = dbuff[k];
			break;
		case 1:
			outvec->y = dbuff[k];
			break;
		case 2:
			outvec->z = dbuff[k];
			break;
		}
		k += 1;
		if (k>2)k = 0;

		cbuff = strtok(NULL, ",");

	}
	return *outvec;
}

//transforms a char variable in V4
VECTOR4F ORIONSERVICEA::CharToVec4(char charvar[MAXLEN], VECTOR4F* outvec){

	double dbuff[4];
	char *cbuff;
	cbuff = NULL;
	cbuff = strtok(charvar, ",");
	int k = 0;
	while (cbuff != NULL)
	{
		int cbs;
		for (cbs = 0; cbs<sizeof(cbuff); cbs++){
			if (cbuff[cbs] == '(') cbuff[cbs] = ' ';
			else if (cbuff[cbs] == ')') cbuff[cbs] = ' ';
		}


		dbuff[k] = atof(cbuff);

		switch (k){
		case 0:
			outvec->x = dbuff[k];
			break;
		case 1:
			outvec->y = dbuff[k];
			break;
		case 2:
			outvec->z = dbuff[k];
			break;
		case 3:
			outvec->t = dbuff[k];
			break;
		}
		k += 1;
		if (k>3)k = 0;

		cbuff = strtok(NULL, ",");

	}
	return *outvec;
}




























