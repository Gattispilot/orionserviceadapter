  /*********************************************************************************************
  This file is part of ORIONSERVICEA project
  Copyright belogs to Fred18 for module implementation and its code
  Biggest Credit goes to Vinka for his idea of Multistage.dll. None of his code was used here since his addons are all closed source.
  Credit goes to Face for having pointed me to the GetPrivateProfileString 
  Credit goes to Hlynkacg for his OrientForBurn function which was the basis on which I developed the Attitude Function.

  ORIONSERVICEA is distributed FREEWARE. Its code is distributed along with the dll. Nobody is authorized to exploit the module or the code or parts of them commercially directly or indirectly.
You CAN distribute the dll together with your addon but in this case you MUST:
-	Include credit to the author in your addon documentation;
-	Add to the addon documentation the official link of Orbit Hangar Mods for download and suggest to download the latest and updated version of the module.
You CAN use parts of the code of ORIONSERVICEA, but in this case you MUST:
-	Give credits in your copyright header and in your documentation for the part you used.
-	Let your project be open source and its code available for at least visualization by other users.
You CAN NOT use the entire code for making and distributing the very same module claiming it as your work entirely or partly.
You CAN NOT claim that ORIONSERVICEA is an invention of yourself or a work made up by yourself, or anyhow let intend that is not made and coded by the author.
You install and use ORIONSERVICEA at your own risk, author will not be responsible for any claim or damage subsequent to its use or for the use of part of it or of part of its code.
*********************************************************************************************/

// ==============================================================
//						ORIONSERVICEA
//                  
//					       By Fred18
//                  
//
// Parser.cpp
// ==============================================================


//#define ORBITER_MODULE

#include <math.h>
#include <stdio.h>
#include "orbitersdk.h"
#include "ORIONSERVICEA.h"





 void ORIONSERVICEA::ArrangePayloadMeshes(char data[MAXLEN], int pnl){
	 std::string meshnm(data);
	
	 std::string meshnm0, meshnm1, meshnm2, meshnm3, meshnm4;
	std::size_t findFirstSC=meshnm.find_first_of(";");
		if(findFirstSC!=meshnm.npos){
		meshnm0=meshnm.substr(0,findFirstSC);
		meshnm0.copy(payload[pnl].meshname0,MAXLEN,0);
		std::size_t findSecondSC=meshnm.find_first_of(";",findFirstSC+1);
			if(findSecondSC!=meshnm.npos){
				meshnm1=meshnm.substr(findFirstSC+1,findSecondSC-findFirstSC-1);
				std::size_t findThirdSC=meshnm.find_first_of(";",findSecondSC+1);
				if(findThirdSC!=meshnm.npos){
					meshnm2=meshnm.substr(findSecondSC+1,findThirdSC-findSecondSC-1);
						std::size_t findFourthSC=meshnm.find_first_of(";",findThirdSC+1);
						if(findFourthSC!=meshnm.npos){
							meshnm3=meshnm.substr(findThirdSC+1,findFourthSC-findThirdSC-1);
							meshnm4=meshnm.substr(findFourthSC+1,meshnm.npos);
							payload[pnl].nMeshes=5;
						}else{  meshnm3=meshnm.substr(findThirdSC+1,meshnm.npos);payload[pnl].nMeshes=4;}
				}else{meshnm2=meshnm.substr(findSecondSC+1,meshnm.npos);payload[pnl].nMeshes=3;}
			}else{meshnm1=meshnm.substr(findFirstSC+1,meshnm.npos);payload[pnl].nMeshes=2;}
		}else{meshnm0=meshnm.substr(0,meshnm.npos);payload[pnl].nMeshes=1;}
		
		meshnm0.copy(payload[pnl].meshname0,MAXLEN,0);
	//	oapiWriteLog(payload[pnl].meshname0);
		if(payload[pnl].nMeshes==5){
		meshnm1.copy(payload[pnl].meshname1,MAXLEN,0);
		meshnm2.copy(payload[pnl].meshname2,MAXLEN,0);
		meshnm3.copy(payload[pnl].meshname3,MAXLEN,0);
		meshnm4.copy(payload[pnl].meshname4,MAXLEN,0);
		}else if(payload[pnl].nMeshes==4){
		meshnm1.copy(payload[pnl].meshname1,MAXLEN,0);
		meshnm2.copy(payload[pnl].meshname2,MAXLEN,0);
		meshnm3.copy(payload[pnl].meshname3,MAXLEN,0);
		}else if(payload[pnl].nMeshes==3){
		meshnm1.copy(payload[pnl].meshname1,MAXLEN,0);
		meshnm2.copy(payload[pnl].meshname2,MAXLEN,0);
		}else if(payload[pnl].nMeshes==2){
		meshnm1.copy(payload[pnl].meshname1,MAXLEN,0);
		}
		//sprintf(logbuff,"0 %s 1 %s 2 %s 3 %s 4 %s",payload[pnl].meshname0,payload[pnl].meshname1,payload[pnl].meshname2,payload[pnl].meshname3,payload[pnl].meshname4);
		//oapiWriteLog(logbuff);
 }

 void ORIONSERVICEA::ArrangePayloadOffsets(char data[MAXLEN], int pnl){
	 std::string offg(data);
	 std::string off0, off1, off2, off3, off4;
	 char coff0[MAXLEN],coff1[MAXLEN],coff2[MAXLEN],coff3[MAXLEN],coff4[MAXLEN];
	 if(payload[pnl].nMeshes==1){
		std::size_t FindClosingParenthesis=offg.find_first_of(")");
		 //off0=offg.substr(0,offg.npos);
		 off0=offg.substr(0,FindClosingParenthesis);
		 off0.copy(coff0,MAXLEN,0);
		 CharToVec(coff0,&payload[pnl].off[0]);
	 //}else if(payload[pnl].nMeshes==2){
	 }else if(payload[pnl].nMeshes>1){
		 std::size_t findFirstSC=offg.find_first_of(";");
		 off0=offg.substr(0,findFirstSC);
		 off0.copy(coff0,MAXLEN,0);
		 CharToVec(coff0,&payload[pnl].off[0]);
			
		 if(payload[pnl].nMeshes==2){
		 off1=offg.substr(findFirstSC+1,offg.npos);
		 off1.copy(coff1,MAXLEN,0);
		 CharToVec(coff1,&payload[pnl].off[1]);
			}else{
				std::size_t findSecondSC=offg.find_first_of(";",findFirstSC+1);
				off1=offg.substr(findFirstSC+1,findSecondSC-findFirstSC-1);
				off1.copy(coff1,MAXLEN,0);
				CharToVec(coff1,&payload[pnl].off[1]);
				if(payload[pnl].nMeshes==3){
					off2=offg.substr(findSecondSC+1,offg.npos);
					off2.copy(coff2,MAXLEN,0);
					CharToVec(coff2,&payload[pnl].off[2]);
				}else{
					std::size_t findThirdSC=offg.find_first_of(";",findSecondSC+1);
					off2=offg.substr(findSecondSC+1,findThirdSC-findSecondSC-1);
					off2.copy(coff2,MAXLEN,0);
					CharToVec(coff2,&payload[pnl].off[2]);
					if(payload[pnl].nMeshes==4){
						off3=offg.substr(findThirdSC+1,offg.npos);
						off3.copy(coff3,MAXLEN,0);
						CharToVec(coff3,&payload[pnl].off[3]);
					}else{
						std::size_t findFourthSC=offg.find_first_of(";",findThirdSC+1);
						off3=offg.substr(findThirdSC+1,findFourthSC-findThirdSC-1);
						off3.copy(coff3,MAXLEN,0);
						CharToVec(coff3,&payload[pnl].off[3]);
						off4=offg.substr(findFourthSC+1,offg.npos);
						off4.copy(coff4,MAXLEN,0);
						CharToVec(coff4,&payload[pnl].off[4]);
					}
				}
		 }
	 }
	 
 }
 void ORIONSERVICEA::parsePayload(char filename[MAXLEN]){
	 char payloadtxt[64];
	char bufftxt[128];

	 int pnl;
	for(pnl=0;pnl<=10;pnl++){
		
sprintf(payloadtxt,"PAYLOAD");
	sprintf(bufftxt,"_%i",pnl+1);
	strcat(payloadtxt,bufftxt);

	payload[pnl].nMeshes=0;
	GetPrivateProfileString(payloadtxt, "meshname", buffreset, dataparsed, MAXLEN, filename);
	
	strcpy(payload[pnl].meshname,dataparsed);
	
	if(payload[pnl].meshname[0]=='0'){
	nPayloads=pnl;
	sprintf(logbuff,"%s: Number of Payloads in the ini file: %i",GetName(),nPayloads);
	oapiWriteLog(logbuff);
		break;	}
	ArrangePayloadMeshes(dataparsed,pnl);
	GetPrivateProfileString(payloadtxt, "off", buffreset, dataparsed, MAXLEN, filename);
	ArrangePayloadOffsets(dataparsed,pnl);
	//CharToVec(dataparsed,&payload[pnl].off[0]);
	GetPrivateProfileString(payloadtxt, "height", buffreset, dataparsed, MAXLEN, filename);
	payload[pnl].height=atof(dataparsed);
	GetPrivateProfileString(payloadtxt, "diameter", buffreset, dataparsed, MAXLEN, filename);
	payload[pnl].diameter=atof(dataparsed);
	GetPrivateProfileString(payloadtxt, "mass", buffreset, dataparsed, MAXLEN, filename);
	payload[pnl].mass=atof(dataparsed);
	GetPrivateProfileString(payloadtxt, "module", buffreset, dataparsed, MAXLEN, filename);
	if(strncmp(dataparsed,buffreset,MAXLEN-5)==0){sprintf(dataparsed,"Stage");}
	//sprintf(payload[pnl].module,dataparsed);
	strcpy(payload[pnl].module,dataparsed);
	GetPrivateProfileString(payloadtxt, "name", buffreset, dataparsed, MAXLEN, filename);
	//sprintf(payload[pnl].name,dataparsed);
	strcpy(payload[pnl].name,dataparsed);
	GetPrivateProfileString(payloadtxt, "speed", buffreset, dataparsed, MAXLEN, filename);
	//payload[pnl].speed=CharToVec(dataparsed);
	CharToVec(dataparsed,&payload[pnl].speed);
	GetPrivateProfileString(payloadtxt, "rot_speed", buffreset, dataparsed, MAXLEN, filename);
	//payload[pnl].rot_speed=CharToVec(dataparsed);
	CharToVec(dataparsed,&payload[pnl].rot_speed);
	GetPrivateProfileString(payloadtxt,"rotation",buffreset,dataparsed,MAXLEN,filename);
	payload[pnl].Rotation=_V(0,0,0);
	payload[pnl].rotated=FALSE;
	CharToVec(dataparsed,&payload[pnl].Rotation);
	payload[pnl].Rotation=operator*(payload[pnl].Rotation,RAD);
	if(length(payload[pnl].Rotation)>0){payload[pnl].rotated=TRUE;}

	GetPrivateProfileString(payloadtxt,"render",buffreset,dataparsed,MAXLEN,filename);
	payload[pnl].render=atoi(dataparsed);
	if(payload[pnl].render!=1){
		payload[pnl].render=0;
	}
	GetPrivateProfileString(payloadtxt,"live",buffreset,dataparsed,MAXLEN,filename);
	int check=atoi(dataparsed);
	if(check==1){payload[pnl].live=TRUE;}else{payload[pnl].live=FALSE;}

	}
	

}

bool ORIONSERVICEA::parseinifile(char filename[MAXLEN]){
	int r;
	for(r=0;r<MAXLEN;r++){
		buffreset[r]='0';
	}
//parseInterstages(filename);
parsePayload(filename);
	return true;
}

