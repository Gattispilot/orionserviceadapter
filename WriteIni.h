#pragma once

#include "orbitersdk.h"

class WriteIni{
	friend class DevModeDlg;
public:
	WriteIni();
	~WriteIni();
bool WritePayload(PAYLOAD pld, UINT idx, bool Erase=FALSE);

//BOOL WNull();
BOOL W(LPCSTR b, LPCSTR c);
BOOL Wvec(LPCSTR b, VECTOR3 v);
BOOL Wdouble(LPCSTR b, double c);
BOOL WLongDouble(LPCSTR b, double c);
BOOL Wint(LPCSTR b, int c);
BOOL Wbool_int(LPCSTR b, bool c);
char* VecToChar(VECTOR3 v);
void SetFilename(char fn[MAXLEN]);
void SetCurrentSection(char Section[MAX_PATH]);
char sectiontxt[MAX_PATH];
private:
	char filename[MAXLEN];
	
};