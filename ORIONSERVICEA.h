/****************************************************************************
This file is part of Multistage2015 project
Copyright belogs to Fred18 for module implementation and its code
Biggest Credit goes to Vinka for his idea of Multistage.dll. None of his code was used here since his addons are all closed source.
Credit goes to Face for having pointed me to the GetPrivateProfileString
Credit goes to Hlynkacg for his OrientForBurn function which was the basis on which I developed the Attitude Function.

Multistage2015 is distributed FREEWARE. Its code is distributed along with the dll. Nobody is authorized to exploit the module or the code or parts of them commercially directly or indirectly.
You CAN distribute the dll together with your addon but in this case you MUST:
-	Include credit to the author in your addon documentation;
-	Add to the addon documentation the official link of Orbit Hangar Mods for download and suggest to download the latest and updated version of the module.
You CAN use parts of the code of Multistage2015, but in this case you MUST:
-	Give credits in your copyright header and in your documentation for the part you used.
-	Let your project be open source and its code available for at least visualization by other users.
You CAN NOT use the entire code for making and distributing the very same module claiming it as your work entirely or partly.
You CAN NOT claim that Multistage2015 is an invention of yourself or a work made up by yourself, or anyhow let intend that is not made and coded by the author.
You install and use Multistage2015 at your own risk, author will not be responsible for any claim or damage subsequent to its use or for the use of part of it or of part of its code.


*/
// ==============================================================
//						MultiStage2015
//                  
//					       By Fred18
//                  
//
// Multistage2015.h
// ==============================================================



#define ORBITER_MODULE

#define MAXLEN 4096
#define TPAYLOAD 2

#include <iostream>
#include <fstream>
#include <string>
#include "windows.h"

#include "orbitersdk.h"

struct VECTOR4F{
	double x;
	double y;
	double z;
	double t;
	VECTOR4F()
	{
		x = 0;
		y = 0;
		z = 0;
		t = 0;
	}
};


struct PAYLOAD{
	//mandatory
	char meshname[MAXLEN];
	char meshname0[MAXLEN];
	char meshname1[MAXLEN];
	char meshname2[MAXLEN];
	char meshname3[MAXLEN];
	char meshname4[MAXLEN];

	VECTOR3 off[5];
	double height;
	double diameter;
	double mass;
	char module[MAXLEN];
	char name[MAXLEN];
	//unmandatory
	char MultiOffset[128];
	VECTOR3 speed;
	VECTOR3 rot_speed;

	double volume;
	int render;
	int nMeshes;
	MESHHANDLE msh_h[5];
	int msh_idh[5];
	VECTOR3 Rotation;
	bool rotated;
	bool live;

	PAYLOAD()
	{
		for (UINT i = 0; i<MAXLEN; i++)
		{
			meshname[i] = '\0';
			meshname0[i] = '\0';
			meshname1[i] = '\0';
			meshname2[i] = '\0';
			meshname3[i] = '\0';
			meshname4[i] = '\0';
		}
		for (int i = 0; i<5; i++){
			off[i] = _V(0, 0, 0);
			msh_h[i] = NULL;
			msh_idh[i] = 0;
		}
		height = 0;
		diameter = 0;
		mass = 0;
		module[0] = '\0';
		name[0] = '\0';
		MultiOffset[0] = '\0';
		speed = _V(0, 0, 0);
		rot_speed = _V(0, 0, 0);
		volume = 0;
		render = 0;
		nMeshes = 0;
		Rotation = _V(0, 0, 0);
		rotated = FALSE;
		live = FALSE;
	}
};




class ORIONSERVICEA : public VESSEL3 {
public:
	ORIONSERVICEA(OBJHANDLE hObj, int fmodel);

	
	void clbkSetClassCaps(FILEHANDLE cfg);
	int clbkConsumeBufferedKey(DWORD key, bool down, char *kstate);
	void clbkLoadStateEx(FILEHANDLE scn, void *vs);
	void clbkSaveState(FILEHANDLE scn);
	void clbkPostStep(double simt, double simdt, double mjd);
	void clbkPreStep(double simt, double simdt, double mjd);
	//void clbkVisualCreated (VISHANDLE _vis, int refcount);
	//void clbkVisualDestroyed (VISHANDLE _vis, int refcount);
	int MyID;
	//VISHANDLE vis;
	void VehicleSetup();
	void RotatePayload(int pns, int nm, VECTOR3 anglesrad);
	void LoadMeshes();
	void UpdateMass();
	void UpdatePMI();
	double TotalHeight;
	void initGlobalVars();
	void UpdateOffsets(); ///////*********
	char dataparsed[MAXLEN];
	bool parseinifile(char filename[MAXLEN]);
	void parsePayload(char filename[MAXLEN]);
	void ArrangePayloadMeshes(char data[MAXLEN], int pnl);
	char* GetProperPayloadMeshName(int pnl, int n);
	void ArrangePayloadOffsets(char data[MAXLEN], int pnl);

	VECTOR3 CharToVec(char charvar[MAXLEN], VECTOR3* outvec);
	VECTOR4F CharToVec4(char charvar[MAXLEN], VECTOR4F* outvec);

	std::string value1, value2, value3, value4, value5, value6;





	int Configuration;
	double CogElev;

	char OrbiterRoot[MAXLEN];
	char fileini[MAXLEN];
	char guidancefile[MAXLEN];
	int nPayloads;
	int currentStage;
	int currentPayload;
	
	
	PAYLOAD payload[13];

	
	char logbuff[MAXLEN];
	char buffreset[MAXLEN];
	double MET;
	SURFHANDLE GetProperExhaustTexture(char name[MAXLEN]);
	void FLY(double simtime, double simdtime, double mjdate);
	VECTOR3 RotateVecZ(VECTOR3 input, double Angle);
	

	//bool wVinkasGuidance;
	VECTOR3 RotateVector(const VECTOR3& input, double angle, const VECTOR3& rotationaxis);
	ATTACHMENTHANDLE live_a[15];
	void UpdateLivePayloads();
	MATRIX3 RotationMatrix(VECTOR3 angles);
	// int nVent;
	double lvl;





	


	void ResetVehicle();
	VECTOR3 hangaranims;
	FILEHANDLE scenario;
	FILEHANDLE config;
	int loadedCurrentBooster;
	int loadedCurrentInterstage;
	int loadedCurrentStage;
	int loadedCurrentPayload;
	int loadedwFairing;
	int loadedConfiguration;
	bool loadedGrowing;
	double loadedMET;
	VESSELSTATUS2 vs2;
	MATRIX3 RotMatrix;
	bool DeveloperMode;
	bool killDMD;
	NOTEHANDLE note;
private:
	MGROUP_ROTATE *payloadrotatex[15][5];
	MGROUP_ROTATE *payloadrotatey[15][5];
	MGROUP_ROTATE *payloadrotatez[15][5];
	UINT RotatePayloadAnim_x[15][5];
	UINT RotatePayloadAnim_y[15][5];
	UINT RotatePayloadAnim_z[15][5];
	ANIMATIONCOMPONENT_HANDLE anim_x[15][5];
	ANIMATIONCOMPONENT_HANDLE anim_y[15][5];
	ANIMATIONCOMPONENT_HANDLE anim_z[15][5];


	void Ramp(bool alreadyramp);
	//bool RampCreated;
	//bool AttachedToRamp;
	
	//bool RampDeleted;
	bool wRamp;
	bool NoMoreRamp;
	int GetMSVersion();


	COLOUR4 col_d;
	COLOUR4 col_s;
	COLOUR4 col_a;
	COLOUR4 col_white;

	double th_main_level;
	double launchFx_level;

	void CreateLaunchFX();

	bool HangarMode;
	bool wCrawler;
	bool wCamera;
	double CamDLat, CamDLng;
	void CreateHangar();
	void CreateCamera();
	bool CheckForDetach();
	bool AttToMSPad;
	void AttachToMSPad(OBJHANDLE hPad);
	VECTOR3 MsPadZ;
	double GetMET();
	bool GetAutopilotStatus();
	VECTOR3 GetAPTargets();

	//virtual void UpdateMeshVisual(UINT *msh_id, VECTOR3 Rotation);
};



