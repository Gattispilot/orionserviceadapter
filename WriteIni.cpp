#include "Orbitersdk.h"
#include "orionservicea.h"
#include "WriteIni.h"

WriteIni::WriteIni(){}
WriteIni::~WriteIni(){}

void WriteIni::SetFilename(char fn[MAXLEN])
{
	strcpy(filename,fn);
}
char* WriteIni::VecToChar(VECTOR3 v)
{
	char out[MAX_PATH];
	sprintf(out,"(%.3f,%.3f,%.3f)",v.x,v.y,v.z);
	
	return out;
}

/*BOOL WriteIni::WNull()
{
	return W(NULL,NULL);
}*/
BOOL WriteIni::W(LPCSTR b, LPCSTR c)
{
	return WritePrivateProfileString(sectiontxt,b,c,filename);
}

BOOL WriteIni::Wvec(LPCSTR b, VECTOR3 v)
{
	char temp[MAX_PATH];
	sprintf(temp,VecToChar(v));
	return W(b,temp);

}
BOOL WriteIni::Wdouble(LPCSTR b, double c)
{
	char temp[MAX_PATH];
	sprintf(temp,"%.3f",c);
	return W(b,temp);
}
BOOL WriteIni::WLongDouble(LPCSTR b, double c)
{
	char temp[MAX_PATH];
	sprintf(temp,"%lg",c);
	return W(b,temp);
}
BOOL WriteIni::Wint(LPCSTR b, int c)
{
	char temp[MAX_PATH];
	sprintf(temp,"%i",c);
	return W(b,temp);
}
BOOL WriteIni::Wbool_int(LPCSTR b, bool c)
{
	char temp[MAX_PATH];
	int q=0;
	if(c){	q=1;}
	sprintf(temp,"%i",q);
	return W(b,temp);
}
void WriteIni::SetCurrentSection(char Section[MAX_PATH])
{
	strcpy(sectiontxt,Section);
}
bool WriteIni::WritePayload(PAYLOAD pld, UINT idx, bool Erase)
{
	
	char sectionpld[MAX_PATH],idxtxt[56];
	sprintf(sectionpld,"PAYLOAD");
	sprintf(idxtxt,"_%i",idx+1);
	strcat(sectionpld,idxtxt);
	
	SetCurrentSection(sectionpld);
	if(!Erase){
	W("Meshname",pld.meshname);
	//Wvec("Off",pld.off[0],filename);
	W("Off",pld.MultiOffset);
	Wvec("Rotation",pld.Rotation);
	Wdouble("Height",pld.height);
	Wdouble("Diameter",pld.diameter);
	Wdouble("Mass",pld.mass);
	W("Module",pld.module);
	W("Name",pld.name);
	Wvec("Speed",pld.speed);
	Wvec("Rot_speed",pld.rot_speed);
	Wint("Render",pld.render);
	Wbool_int("Live",pld.live);
	}else{
	W(NULL,NULL);
	}
	
	return TRUE;
}

